# Tar Usage / Cheat Sheet

**NOTE:**  You do not need a dash for tar commands. If using a - in a tar command f must be the last option or you will most likey get an error.

## Compress a file or directory

e.g: `tar -czvf name-of-archive.tar.gz /path/to/directory-or-file`

* -c: Create an archive.
* -z: Compress the archive with gzip.
* -v: makes tar talk a lot. Verbose output shows you all the files being archived and much.
* -f: Allows you to specify the filename of the archive.

## Extract an Archive

e.g: `tar -xvzf name-of-archive.tar.gz`


* f: this must be the last flag of the command, and the tar file must be immediately after. It tells tar the name and path of the compressed file.
* z: tells tar to decompress the archive using gzip
* x: tar can collect files or extract them. x does the latter.
* v: makes tar talk a lot. Verbose output shows you all the files being extracted.


## More Examples
*Create tar archives:*

Plain tar archive: 

`tar -cf archive.tar file1 file2 file3`

Gzipped tar archive: 

`tar -czf archive.tgz file1 file2 file3`

Bzipped tar archive: 

`tar -cjf archive.tbz file1 file2 file3`



Using Wildcards:

`tar -cf backup.tar “*.xml”`

Splitting the archive (makes 200MB backups from the /dir folder.)

 `tar cvf - /dir | split --bytes=200MB - backup.tar`


*Extract tar archives:*

Plain tar archive: 

`tar -xf archive.tar`

Gzipped tar archive: 

`tar -xzf archive.tgz`

Bzipped tar archive: 

`tar -xjf archive.tbz`


## Append files to existing archives
Files or directories can be added/updated to the existing archives using r flag. Take a look at the following command.

`$ tar rf my.tar dir/ dir2/ example.txt`

## Create an encrypted archive:

`tar -cvzf - folder | gpg -c > folder.tar.gz.gpg` 
or less secure version
`tar -cvzf - folder | gpg -c --passphrase yourpassword > folder.tar.gz.gpg`

Decrypt encrypted archive

`gpg -d folder.tar.gz.gpg | tar -xvzf -`


Create,encrypt and transmit an archive over netcat  (no scp no problem)

` tar -cvzf - folder | gpg -c | nc -l 6666`



List contents of archive files without extracting them
To list the contents of an archive file, we use t flag.

`$ tar tf my.tar `

Exclude directories and/or files from while creating an archive
This is quite useful when backing up your data. You can exclude the non-important files or directories from your backup. This is where --exclude switch comes in help. For example, you want to create an archive of your /home directory, but exclude Downloads, Documents, Pictures, Music directories.


`$ tar czvf my.tgz /home/rp --exclude=/home/rp/Downloads --exclude=/home/rp/Documents --exclude=/home/rp/Pictures --exclude=/home/rp/Music`



Create archive of multiple directories and/or files at a time
This is another coolest feature of tar command. To create an gzipped archive of multiple directories or files at once, use this command:

`$ tar czvf ostechnix.tgz Downloads/ Documents/ ostechnix/file.odt`



multithreaded compression (needs 'pigz' installed):

`tar --use-compress-program=pigz -cf <file.tar.gz> <path-or-file>`

short hand:

`tar -I pigz -cf <file.tar.gz> <path-or-file>`




*Test an archive is not corrupt*
`tar -tvWF backup.tar`



