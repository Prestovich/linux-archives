# linux archives

Demo of various archiving utilities on linux

Archive, compress, unpack,  uncompress and encrypt files using tar, star, gzip, and bzip2



## **cpio**  
General purpose file archiver stands for Copy in and out. Actively used by RedHat Package Manager (RPM) and in the initramfs 
```
      ./directory_generator.sh test
      find . -iname test/R* test/E* test/D* test/H* test/A* test/T* -type f -print0 | cpio --create > ./files.cpio
      cpio -idv < files.cpio
```

## **(un)zip**

Simple commands to zip and unzip
```
      zip OPTIONS ARCHIVE_NAME FILES
      #encrypt file
      zip -e  archivename.zip file1 file2 filen
      #encrypt dir
      zip -er  archivename.zip directory_name
      unzip archivename.zip

```
### **gzip** && **bzip2** 

[gzip](https://www.geeksforgeeks.org/gunzip-command-in-linux-with-examples/) gunzip, and zcat based off Lempel-Ziv (LZ77) algorithm used in zip and PKZIP.

[bzip2](https://www.geeksforgeeks.org/bzip2-command-in-linux-with-examples/) based off Burrows-Wheeler block sorting text compression algorithm
```
      #gunzip and gzip same command without specifying compression or archive
      cd test/numbers
      gzip 1.txt 2.txt
      gunzip 1.txt.gz 2.txt.gz

      cd test/letters
      bzip2 -z a.txt
      bzip2 -d a.txt.bz2

```

#### Benchmarks *gzip* vs *bzip*  [2005](https://tukaani.org/lzma/benchmarks.html)  [2015](https://www.rootusers.com/gzip-vs-bzip2-vs-xz-performance-comparison/)


Winner depends... 




## **ar**  

Utility for archives replaced by tar. Now used mostly to update static libs. [tut](https://www.geeksforgeeks.org/ar-command-in-linux-with-examples) 
``` 
      head -c 100m /dev/zero | openssl enc -aes-128-cbc -pass pass:"$(head -c 20 /dev/urandom | base64)"  > teststar1
      ar r super.a *star1
      rm *star1
      ar xv super.a
```
## **shar** 

Not available in any default repos .  Legacy Shar has an advantage of being plain text however it is potentially dangerous, since it outputs an executable.
```
      shar file_name.extension > filename.shar
      unshar file_name.shar
```

## **star** 

Super tar? Not available by default in Fedora. Original and possibly only utility that implemented *POSX.1-1998* and [POSIX.1-2001](https://www.systutorials.com/docs/linux/man/5-star/) as oringal tar was designed in 85 and complied to the POSIX-1003.1-1988 standard. Supports:
- All tar archiving formats with options for 7zip, gzip, bzip2 
- Similar tar functionality such as diff, acls, snapshots, synchronizing filesystems and backup scheduling
- Can support the older smaller tar implimentation 2GB limit and can write up to 8GB files on gnu systems and supposed unlimited in POSIX
- Most robust tool for dealing with magnetic tape

```
man star

```

## **tar**
Tar is the bar for archiving on linux

Which of the following will throw an error? Why?
```
tar -fcvz ./mtest.tar.gz /test/letters

tar fcvz ./mtest.tar.gz /test/letters
```


- [GNU manual](https://www.gnu.org/software/tar/manual/html_chapter/index.html#Top)
- [Formats](https://www.gnu.org/software/tar/manual/html_chapter/Formats.html)




## Alternate Cross platform Popular Archiving tools
- [7zip](https://www.linuxlinks.com/7-Zip/) 
- [PeaZip](https://peazip.github.io/peazip-linux.html)
- [winzip](https://www.winzip.com/win/en/downwz.html) fips compliant encryption


## Algos
- gzip 
- bzip2
- lzip
- lzma 
- lzop
- zstd
- xz  
- traditional compress


